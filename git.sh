# preparar para commit todo
git add .
git add --all

# commit all
git commit -m "mensaje del commit"

# add and commit staged files
git commit -am "mensaje del commit"

# subir cambios al sevidor
git push

# cambiar mensaje del ultimo commit
# saldra un editor, presione INSERT para cambiar
# presione ESC + :wq para guardar y salir
git commit --amend

# si el commit ya estaba publicado
# de esta manera se actualiza el commit publicado
git push --force

###################################################################################################################

# listar ramas remotas
git branch -r
git ls-remote --heads

# mezclar ramas, debes pararte primero encima de la rama con la que deseas mezclar
git merge <nombre-rama>

# regresar el merge
git reset --hard HEAD

# crear rama
git branch <nombre-rama>

# crear rama y cambiar de una vez
git checkout -b <nombre-rama>

# cambiar de rama
git checkout <nombre-rama>

# para subir la rama al repositorio
git push origin <nombre-rama>

# para bajar y subir cambios de rama
git pull origin <nombre-rama>
git push origin <nombre-rama>
git push --set-upstream origin <nombre-rama>

# eliminar rama
git branch -d feature/optimizacionLibrerias

# cuando eliminas una rama, para subir los cambios
git push origin :feature/optimizacionLibrerias

###################################################################################################################
# para hacer persistente la eliminación de un archivo además
git add --all
git commit -m "message"
git push

# descartar cambios hechos y bajarse del servidor
git reset --hard
git pull origin

# volver a un commit anterior (remove untracked files)
git reset --hard 4a155e5
git reset --hard <SHAsum of your commit>

# clonar repositorio, luego cambiar a la carpeta
git init
git clone <url>

# volver al commit anteior
git reset --HARD ####

# volver al commit anterior local y remoto
git reset --HARD <hash_commit>
git push --force origin

# eliminar archivos no agregados
git clean -f -d

# view changes
git diff

# view changes in file
git diff file_path

# for view history by file
gitk
gitk [filename]
gitk --follow [filename]

# show config values
git config --list

# ver lista de tags
git tag -l

############################################################################################################

# crear una etiqueta en un determinado commit
git tag -a v4.25.0-RELEASE -m 'version 4.25.0-RELEASE' <hash_commit>
git tag -a v3.3.0-RELEASE -m 'version 3.3.0-RELEASE' 86b24f5e4746e3051430309f3f228ee81c9cc36f

# compartir o propagar la etiqueta al servidor remoto
git push origin v1.2

# si tienes un monton de etiquetas que quieres enviar a la vez
git push origin --tags

# eliminar tag
git tag -d <tag_name>
git push origin :<tag_name>

git remote set-url origin <repo uri>
git push --all origin

# avoid (https://stackoverflow.com/questions/1257592/how-do-i-remove-files-saying-old-mode-100755-new-mode-100644-from-unstaged-cha)
# old mode 100755
# new mode 100644
git config core.filemode false