#!/bin/bash

# crear una etiqueta en un determinado commit
# git tag -a v1.2 -m 'version 1.2' 9fceb02

# compartir o propagar la etiqueta al servidor remoto
# git push origin v1.2

# si tienes un monton de etiquetas que quieres enviar a la vez
# git push origin --tags

# ruta desde donde se ejecuta el script
HOME="$PWD"
# HOME="/c/Users/mquintosa"

BUILD_DIR="$HOME/portal-solicitud-root-ws/portal-solicitud-ws/build/libs"
BUILD_DIR_PORTAL_LIFERAY="$HOME/portal-integracion-cms-portlets/portal-integracion-portlets/build/libs"
BUILD_DIR_NG="$HOME/portal-solicitudes-v2-ng"
BUILD_DIR_SOLICITUDES_GESTAH_NG="$HOME/tthh-gestah-solicitudes-v2-ng"
BUILD_DIR_AUTO_PAGO="$HOME/autorizacion-pago-prestamo-root-ws/autorizacion-pago-prestamo-ws/build/libs"
BUILD_DIR_TAREAS_PROGRAMADAS="$HOME/tthh-gestah/dependencias-ear"
DEPLOY_DIR="$HOME/server-gestah/jboss-eap-6.2.3/standalone/deployments"
LOGS_DIR="$HOME/server-gestah/jboss-eap-6.2.3/standalone/log"
STANDALONE_DIR_GESTAH_PORTAL="/opt/rh/jboss/jboss-eap-6.2.3_gestah_portal_1_8780/standalone"
STANDALONE_DIR_PORTAL_MODULES="/opt/portal-modules/jboss-eap-6.2.3/standalone"
STANDALONE_DIR_TAREAS_PROGRAMADAS_GESTAH="/opt/rh/jboss/jboss-eap-6.2.3_stp_tthh_1/standalone"

GRADLE_DIR31="/c/gradle-3.1/bin"
GRADLE_DIR35="/c/gradle-3.5/bin"

# servidor portal liferay
LABEL_SERVER_MJ291K6="\033[1;32msvr-mj291k6\033[0m"
# servidor gestah portal
LABEL_SERVER_MJ0359RV="\033[1;32msvr-mj0359rv\033[0m"
# servidor corporativo
LABEL_SERVER_221052D="\033[1;32msvr-221052d\033[0m"

MESSAGE_NOT_OPTION="\n\n${RED}La opcion seleccionada no existe, o ya se caduco el tiempo de espera.${NC}"
MESSAGE_SELECT="Ruta actual: \033[1;36m$PWD\033[0m\n\tHola \033[1;31mMICHE\033[0m :), selecciona una opción:"

#  Black        0;30     Dark Gray     1;30
#  Red          0;31     Light Red     1;31
#  Green        0;32     Light Green   1;32
#  Brown/Orange 0;33     Yellow        1;33
#  Blue         0;34     Light Blue    1;34
#  Purple       0;35     Light Purple  1;35
#  Cyan         0;36     Light Cyan    1;36
#  Light Gray   0;37     White         1;37

WHITE='\033[1;37m'
RED='\033[1;31m'
BLUE='\033[1;34m'
YELLOW='\033[1;33m'
CYAN='\033[1;36m'
# No Color
NC='\033[0m'

# para deplegar la app angular
# $ gulp serve:bundle

# para generar
# $ gulp deploy:dist

# debe utilizar la consola de git-bash para ejecutarlo, algo así como: $ ./mypath/generate_solicitudes_ws.sh

# Para evitar que te pida el password cada vez que te conectes al server (unix) de PRE
# sigue los siguientes pasos:
# 1 - si nunca has agregado un ssh-key ejecuta (presione 'y' enter sin escribir nada mas): ssh-keygen -t rsa -b 2048
# 2 - guarda la clave en ssh-key ejecutando el siguente comando (le pide la clave, escribala y enter): ssh-copy-id user@server
# 3 - listo, ahora cuando ejecutas ssh user@server no pedira la clave, jejejeje, mejor

# Ventajas:
# 1 - En un lugar está todo lo que necesitas hacer para desplegar, no tienes que abrir más ventanas
# 2 - Puedes ir haciendo otra cosa mientras se ejecuta el proceso de despliegue (todo se hará)
# 3 - No tienes que estar recordando el nombre del servidor donde vas a desplegar tu aplicación, ni la clave, ya está aquí
# 4 - No tienes que estar esperando porque se genere uno para generar el otro, ni para desplegar, es un único proceso
# 5 - Por todo esto te ahorras 5 min, por cada despligue que haces, y además no cometes los mismos errores otra vez
# (el burro no tropieza con la misma piedra dos veces, si quieres no lo hagas, tu no eres burro)
# 7 - Puedes mejorarlo, desde aquí, o agregar otros procesos, y puedes ahorrar más tiempo (siiii)
# Ah, si ejecutas los tests antes generar y desplegar puedes darte cuenta de los errores que puedas tener

showMainMenu() {
	clear
	echo -e "
	${MESSAGE_SELECT}

${YELLOW}DESARROLLO -----------------------------------------------------------------------------------------------------------------

	1   - Generar tthh-root
	2   - Generar tthh-portal
	21  - Generar personal-core
	3   - Generar portal-solicitud-root-ws
	3a  - GENERAR-DESPLEGAR portal-solicitud-root-ws
	4   - Generar tthh-root, tthh-portal, portal-solicitud-root-ws y desplegar
	4a  - Generar tthh-portal, portal-solicitud-root-ws y desplegar
	  
	5   - Generar TODOS
	6   - Generar todos y desplegar JBOSS

	7a  - Desplegar portal-solicitud-ws.war JBOSS\n
	7b  - Desplegar nomina-web.war LOCALHOST
	8a  - Ver logs Localhost8480 JBOSS
	  
	9a  - Generar portal-solicitudes (WEB) y desplegar
	9b  - Generar gestah-solicitudes (WEB) y desplegar

	9x  - Generar personal-core, autorizacion-pago-ws y desplegar
	9y  - Generar autorizacion-pago-ws y desplegar

	10  - Generar MIPORTAL PORTLETS y desplegar${NC}

${CYAN}PREPROD -------------------------------------------------------------------------------------------------------------------

	r1  - Reiniciar GESTAH PORTAL MODULES $LABEL_SERVER_MJ0359RV${CYAN}

	30  - Generar TODO solicitudes
	31  - Desplegar TODO solicitudes

	32  - Generar portal-solicitudes (WEB) y desplegar PORTAL-MODULES\n
	35  - Generar portal-solicitudes-gestah (WEB)
	35a - Generar portal-solicitudes-gestah (WEB) y desplegar en GESTAH-PORTAL
	35b - Desplegar portal-solicitudes-gestah (WEB) en GESTAH-PORTAL\n
	40  - Generar portal-solicitud-ws y desplegar GESTAH PORTAL\n
	41  - Desplegar portal-solicitud-ws GESTAH PORTAL\n
	50  - Generar dependencias y desplegar stpTTHH-web TAREAS PROGRAMADAS\n
	51  - Desplegar stpTTHH-web TAREAS PROGRAMADAS $LABEL_SERVER_221052D${CYAN}
	55  - Generar todo SOLICITUDES y DESPLEGAR
	  
	62c - Ver logs GESTAH-PORTAL $LABEL_SERVER_MJ0359RV${CYAN} JBOSS
	62d - Ver logs GESTAH-PORTAL $LABEL_SERVER_MJ0359RV${CYAN} TTHH

	70a - Generar autorizacion-pago-ng y desplegar
	70b - Generar autorizacion-pago-ws y desplegar
	70c - Generar autorizacion-pago-ng, autorizacion-pago-prestamo-ws y desplegar
	70d - Desplegar autorizacion-pago-ng
	70e - Desplegar autorizacion-pago-prestamo-ws

	90  - Actualizar plantillas ${NC}$LABEL_SERVER_MJ0359RV

	m1 - Menú portal

=========================================================================================================================="

read -t120 -n3 -p $"
Escriba la opción a ejecutar (2min)? " key

case $key in
	"1")
	    generateTTHHROOT
	    showMainMenu
	    ;;
	"2")
		generateTTHHPORTAL
		showMainMenu
		;;
	"21")
		generatePersonalCore
		showMainMenu
		;;
	"3")
		generateSolicitudesWS
		;;
	"3a")
		generateSolicitudesWS
		deployAppSolicitudesWS
		logsLocalhost8480
		;;
	"4")
		generateTTHHROOT
		generateTTHHPORTAL
		generateSolicitudesWS
		deployAppSolicitudesWS
		logsLocalhost8480
		;;
	"4a")
		generateTTHHPORTAL
		generateSolicitudesWS
		deployAppSolicitudesWS
		logsLocalhost8480
		;;
	"5")
		generateTTHHROOT
		generateTTHHPORTAL
		generatePersonalCore
		generateSolicitudesWS
		generateAutorizacionPagoWS
		;;
	"6")
		generateTTHHROOT
		generateTTHHPORTAL
		generateSolicitudesWS
		deployAppSolicitudesWS
		;;
	"7a")
		deployAppSolicitudesWS
		logsLocalhost8480
		;;
	"7b")
		generateNominaWeb
		deployAppNominaWebDES
		logsLocalhost8480
		;;
	"8a")
		logsLocalhost8480
		;;
	"9a")
		generateSolicitudesWebDES
		deployAppSolicitudesWebDES
		;;
	"9b")
		generateSolicitudesGestahWebDES
		deployAppSolicitudesGestahWebDES
		;;
	"9x")
		generatePersonalCore
		generateAutorizacionPagoWS
		deployAutorizacionPagoWS_DES
		logsLocalhost8480
		;;
	"9y")
		generateAutorizacionPagoWS
		deployAutorizacionPagoWS_DES
		logsLocalhost8480
		;;
	"r1")
		reiniciarGestahPortalServerPRE
		logsGestahPortalPRE
		;;
	"30")
		generateSolicitudesWebPRE
		generateSolicitudesGestahWebPRE
		generateTTHHROOT
		generateTTHHPORTAL
		generateSolicitudesWS
		;;
	"31")
		deployAppSolicitudesWS
		deployAppSolicitudesWSPRE
		deployAppSolicitudesWebPRE
		deployAppSolicitudesGestahWebPRE
		logsPortalModulesPRE
		;;
	"32")
		generateSolicitudesWebPRE
		deployAppSolicitudesWebPRE
		logsPortalModulesPRE
		;;
	"35")
		generateSolicitudesGestahWebPRE
		;;
	"35a")
		generateSolicitudesGestahWebPRE
		deployAppSolicitudesGestahWebPRE
		logsGestahPortalPRE
		;;
	"35b")
		deployAppSolicitudesGestahWebPRE
		logsGestahPortalPRE
		;;
	"40")
		generateSolicitudesWS
		deployAppSolicitudesWSPRE
		logsGestahPortalPRE
		;;
	"41")
		deployAppSolicitudesWSPRE
		
		;;
	"50")
		generateTTHHROOT
		generateTTHHPORTAL
		generatePersonalCore
		generateTAREASPROGRAMADAS
		deployAppTAREASPROGRAMADAS
		;;
	"51")
		deployAppTAREASPROGRAMADAS
		;;
	"55")
		generateSolicitudesGestahWebPRE
		deployAppSolicitudesGestahWebPRE

		generateSolicitudesWebPRE
		deployAppSolicitudesWebPRE

		generateTTHHROOT
		generateTTHHPORTAL
		generateSolicitudesWS
		deployAppSolicitudesWSPRE
		logsGestahPortalPRE
		;;
	"70a")
		generateAutorizacionPagoWeb_PRE
		deployAutorizacionPagoWEB_PRE
		logsGestahPortalPRE
		;;
	"70b")
		generateAutorizacionPagoWS
		deployAutorizacionPagoWS_PRE
		logsGestahPortalPRE
		;;
	"70c")
		generateAutorizacionPagoWeb_PRE
		deployAutorizacionPagoWEB_PRE
		generateAutorizacionPagoWS
		deployAutorizacionPagoWS_PRE
		logsGestahPortalPRE
		;;
	"70d")
		deployAutorizacionPagoWEB_PRE
		logsGestahPortalPRE
		;;
	"70e")
		deployAutorizacionPagoWS_PRE
		logsGestahPortalPRE
		;;
	"90")
		actualizarPlantillasPRE
		;;
	"m1")
		showPortalMenu
		;;
	*)
		echo -e "$MESSAGE_NOT_OPTION"
esac

}

# menu para opciones del portal
showPortalMenu() {
	clear
	echo -e "
	${YELLOW}PORTAL LIFERAY Y MODULOS${NC}
	
	${MESSAGE_SELECT}

${YELLOW}DESARROLLO ----------------------------------------------------------------------------------------------------------------

	a1  - Generar MIPORTAL PORTLETS y desplegar
	pbl - PORTAL BACKUP LIFERAY

${CYAN}PREPROD -------------------------------------------------------------------------------------------------------------------

	g1  - Generar portlets LIFERAY
	g2  - Generar y desplegar portlets LIFERAY
	d3  - Desplegar portlets LIFERAY

	r1  - Reiniciar LIFERAY PORTAL $LABEL_SERVER_MJ291K6$CYAN
	r2  - Reiniciar MODULES PORTAL $LABEL_SERVER_MJ291K6$CYAN

	l1  - Ver logs Portal LIFERAY $LABEL_SERVER_MJ291K6${CYAN}
	l2  - Ver logs Portal MODULES $LABEL_SERVER_MJ291K6${CYAN}

==========================================================================================================================${NC}"

read -t120 -n3 -p $"
Escriba la opción a ejecutar (2min - regresa)? " key

case $key in

	"pbl")
		generateBackupLiferay
		;;
	"g1")
		generatePortalPortletsLiferay
		removePortalPortletDeployFiles
		deployPortalLiferayPRE
		logsPortalPRE
		;;
	"g2")
		generatePortalPortletsLiferay
		;;
	"d3")
		deployPortalLiferayPRE
		logsPortalPRE
		;;
	"r1")
		reiniciarPortalPRE
		logsPortalPRE
		;;
	"r2")
		reiniciarPortalModulesServerPRE
		logsPortalModulesPRE
		;;
	"l1")
		logsPortalPRE
		;;
	"l2")
		logsPortalModulesPRE
		;;
	*)
		showMainMenu
esac

}

copyJSMPACKAGEDependencies() {
	DIR_FROM="$HOME/tthh-gestah-solicitudes-v2-ng/public/jspm_packages/npm"
	DIR_TO="../../../dist/jspm_packages/npm"
	#cd "$DIR_FROM"
	#cp --parents "ckeditor@4.5.7/contents.css" "$DIR_TO"
	#cp --parents "ckeditor@4.5.7/config.js" "$DIR_TO"
	#cp --parents "ckeditor@4.5.7/styles.js" "$DIR_TO"
	#cp --parents "ckeditor@4.5.7/lang/es.js" "$DIR_TO"
	#cp --parents "ckeditor@4.5.7/skins/moono/icons.png" "$DIR_TO"
	#cp --parents "ckeditor@4.5.7/skins/moono/editor.css" "$DIR_TO"
	#cp --parents "ckeditor@4.5.7/plugins/notification/lang/en.js" "$DIR_TO"
	#cp --parents "ckeditor@4.5.7/plugins/notification/plugin.js" "$DIR_TO"
	#cp --parents "ckeditor@4.5.7/plugins/wordcount/plugin.js" "$DIR_TO"
	#cp --parents "ckeditor@4.5.7/plugins/wordcount/lang/es.js" "$DIR_TO"
}

actualizarPlantillasPRE() {
	echo $'\nActualizando plantillas PRE\n'
	scp /d/SmxLibrerias/Archivos/RRHH/solicitudes-cambios-vacantes/templates/* soporte@svr-mj0359rv:/var/smx/archivos/RRHH/solicitudes-cambios-vacantes/templates
}

logsLocalhost8480() {
	echo $'\n'
	tail -f "$LOGS_DIR"/server.log
}

logsLocalhost8480_TTHH() {
	echo $'\n'
	tail -f -n 70 "$HOME"/Logs/RRHH.log
}

logsPortalModulesPRE() {
	echo $'\n'
	ssh soporte@svr-mj291k6 tail -f -n 50 "$STANDALONE_DIR_PORTAL_MODULES"/log/server.log	
}

logsGestahPortalPRE() {
	echo $'\n'
	ssh soporte@svr-mj0359rv tail -f -n 50 "$STANDALONE_DIR_GESTAH_PORTAL"/log/serverOut
}

logsGestahPortalPRE_TTHH() {
	echo $'\n'
	ssh soporte@svr-mj0359rv tail -f -n 100 /var/smx/archivos/logs/8780/RRHH/RRHH.log
}

reiniciarLocalhostServer() {
	echo $'\nReiniciando el servidor localhost\n'
	echo $'No implementado aún\n'
	# ohhh, todavía no sé, pero si creo que se puede con esto y con wildfly, ejecutar:
	# jboss-cli.sh --connect shutdown
	# o jboss-cli.sh --connect reload
	# o ./path/standalone.sh &
	# pero de momento, podemos dar ctrl + c, y volver a ejecutar, jejeje
}

reiniciarPortalModulesServerPRE() {
	echo $'\nReiniciando el servidor del Portal Modules PRE\n'
	ssh -t soporte@svr-mj291k6 sudo systemctl stop portal-modules.service
	ssh -t soporte@svr-mj291k6 sudo systemctl start portal-modules.service
}

reiniciarGestahPortalServerPRE() {
	echo $'\nReiniciando el servidor de Gestah Portal PRE\n'
	ssh -t soporte@svr-mj0359rv sudo systemctl stop gestah_portal_1_8780.service
	ssh -t soporte@svr-mj0359rv sudo systemctl start gestah_portal_1_8780.service
}

###############################################################################################
#
#			PORTAL LIFERAY
#
###############################################################################################

generateBackupLiferay() {
	cd "$HOME"
	TODAY=$(date '+%Y%m%d')
	DIR_TO_CREATE="server-liferay-backup/$TODAY"
	mkdir -p "$DIR_TO_CREATE"
	# comprimir la carpeta del portal, y ademas excluir las carpetas de log, tmp, data
	tar --exclude "./server-liferay/backup" --exclude "./server-liferay/jboss-eap-6.2.3/standalone/data" --exclude "./server-liferay/jboss-eap-6.2.3/standalone/log" --exclude "./server-liferay/jboss-eap-6.2.3/standalone/tmp" -zcvf "$DIR_TO_CREATE"/liferay_backup.tar.gz ./server-liferay
}

generatePortalPortletsLiferay() {
	echo $'\n\nGenerando los Portlets\n'
	# put jdk 7
	cp /c/Users/mquintosa/.gradle/gradle7.properties /c/Users/mquintosa/.gradle/gradle.properties
	cd "$HOME"/portal-integracion-cms-portlets/portal-integracion-portlets
	"$GRADLE_DIR35"/gradle clean build -x test
	# restore jdk 8
	cp /c/Users/mquintosa/.gradle/gradle8.properties /c/Users/mquintosa/.gradle/gradle.properties
}

removePortalPortletDeployFiles() {
	echo $'\nEliminando portlets del portal\n'
	ssh -t soporte@svr-mj291k6 chmod -R 777 /opt/liferay_2/jboss-eap-6.2.3/standalone/deployments/PortalTTHH-Portlets*
	ssh -t soporte@svr-mj291k6 sudo rm -rf /opt/liferay_2/jboss-eap-6.2.3/standalone/deployments/PortalTTHH-Portlets*
}

deployPortalLiferayPRE() {
	echo $'\nSubiendo Portlets WAR en el JBOSS de PRE (svr-mj291k6) \n'
	FILE="$HOME/portal-integracion-cms-portlets/portal-integracion-portlets/build/libs/PortalTTHH-Portlets.war"
	scp "$FILE" soporte@svr-mj291k6:/opt/liferay_2/temp
	ssh -t soporte@svr-mj291k6 chmod 777 /opt/liferay_2/temp/PortalTTHH-Portlets.war
	ssh -t soporte@svr-mj291k6 mv /opt/liferay_2/temp/PortalTTHH-Portlets.war /opt/liferay_2/deploy/PortalTTHH-Portlets.war
}

reiniciarPortalPRE() {
	echo $'\nDeteniendo el servidor del Portal LIFERAY PRE\n'
	ssh -t soporte@svr-mj291k6 sudo systemctl stop liferay_2.service
	echo $'\nReiniciando el servidor del Portal LIFERAY PRE\n'
	ssh -t soporte@svr-mj291k6 sudo systemctl start liferay_2.service
}

logsPortalPRE() {
	echo $'\n'
	ssh soporte@svr-mj291k6 tail -f -n 550 /opt/liferay_2/jboss-eap-6.2.3/standalone/log/server.log
}

###############################################################################################
#
###############################################################################################

generateTTHHROOT() {
	echo $'\n'
	cd "$HOME"/tthh-root
	"$GRADLE_DIR31"/gradle clean build install -x test
}

generatePersonalCore() {
	echo $'\n'
	cd "$HOME"/tthh-ficha-personal
	"$GRADLE_DIR31"/gradle clean build install -x test
}

generateTTHHPORTAL() {
	echo $'\n'
	cd "$HOME"/tthh-portal
	"$GRADLE_DIR31"/gradle clean build install -x test
}

generateSolicitudesWS() {
	echo $'\n'
	DIR="$HOME"/portal-solicitud-root-ws
	if [ -d $DIR ]; then
		cd "$DIR"
		"$GRADLE_DIR31"/gradle clean build -x test
	else
		echo $'No existe el directorio $HOME, por favor revise'
	fi
}

generateTAREASPROGRAMADAS() {
	echo $'\n\nGenerando TAREAS PROGRAMADAS\n'
	cd "$BUILD_DIR_TAREAS_PROGRAMADAS"
	"$GRADLE_DIR31"/gradle clean build -x test
}

generateSolicitudesWebDES() {
	echo $'\n\nGenerando SOLICITUDES PORTAL WEB para DES\n'
	cd "$BUILD_DIR_NG"
	#gulp deploy:dist --env dev
	gulp deploy --env dev
}

generateSolicitudesWebPRE() {
	echo $'\n\nGenerando SOLICITUDES PORTAL WEB para PRE\n'
	cd "$BUILD_DIR_NG"
	#gulp deploy:dist --env test
	gulp deploy --env test
}

deployAppSolicitudesWebDES() {
	echo $'\nCopiando PORTAL-SOLICITUDES.WAR al JBOSS de DES (server-gestah)\n'
	#cp "$BUILD_DIR_NG"/dist/portal-solicitudes*.war "$DEPLOY_DIR"
	cp "$BUILD_DIR_NG"/public/portal-solicitudes*.war "$DEPLOY_DIR"
}

generateSolicitudesGestahWebDES() {
	echo $'\n\nGenerando aplicacion WEB (DEV) Solicitudes GESTAH\n'
	cd "$BUILD_DIR_SOLICITUDES_GESTAH_NG"
	#gulp deploy:dist --env dev
	gulp deploy --env dev
}

deployAppSolicitudesGestahWebDES() {
	echo $'\nSubiendo WEB WAR al JBOSS de DES (Localhost:8480)\n'
	#cp "$BUILD_DIR_SOLICITUDES_GESTAH_NG"/dist/tthh-gestah-solicitudes-ng*.war "$DEPLOY_DIR"
	cp "$BUILD_DIR_SOLICITUDES_GESTAH_NG"/public/tthh-gestah-solicitudes-ng*.war "$DEPLOY_DIR"
}

generateSolicitudesGestahWebPRE() {
	echo $'\n\nGenerando aplicacion WEB (TEST) Solicitudes GESTAH\n'
	copyJSMPACKAGEDependencies
	cd "$BUILD_DIR_SOLICITUDES_GESTAH_NG"

	# guardar con otro nombre system.config.js para evitar tener que volver a hacer npm install
	cp ./public/system.config.js ./public/system.config.gulp.js
	
	gulp deploy:dist --env test
	#gulp deploy --env test

	# restauro system.config.js solo hago gulp
	mv ./public/system.config.gulp.js ./public/system.config.js
}

deployAppSolicitudesGestahWebPRE() {
	echo $'\nSubiendo WEB WAR al JBOSS de PRE (svr-mj0359rv)\n'
	scp "$BUILD_DIR_SOLICITUDES_GESTAH_NG"/dist/tthh-gestah-solicitudes-ng*.war soporte@svr-mj0359rv:"$STANDALONE_DIR_GESTAH_PORTAL"/deployments/
	#scp "$BUILD_DIR_SOLICITUDES_GESTAH_NG"/public/tthh-gestah-solicitudes-ng*.war soporte@svr-mj0359rv:"$STANDALONE_DIR_GESTAH_PORTAL"/deployments/
}

# aplicacion web del gestah
generateNominaWeb() {
	echo $'\n'
	DIR="$HOME"/tthh-gestah/nomina-web
	if [ -d $DIR ]; then
		cd "$DIR"
		"$GRADLE_DIR31"/gradle clean build -x test
	else
		echo $'No existe el directorio $HOME, por favor revise'
	fi
}

deployAppSolicitudesWS() {
	echo $'\nCopiando WAR SOLICITUDES en el JBOSS\n'
	cp "$BUILD_DIR"/portal-solicitud-ws.war "$DEPLOY_DIR"
}

deployAppSolicitudesWSPRE() {
	echo $'\nSubiendo WS WAR en el JBOSS de PRE (svr-mj0359rv) \n'
	scp "$BUILD_DIR"/portal-solicitud-ws.war soporte@svr-mj0359rv:"$STANDALONE_DIR_GESTAH_PORTAL"/deployments/
}

deployAppSolicitudesWebPRE() {
	echo $'\nSubiendo WEB WAR al JBOSS de PRE (svr-mj291k6)\n'
	#scp "$BUILD_DIR_NG"/dist/portal-solicitudes*.war soporte@svr-mj291k6:"$STANDALONE_DIR_PORTAL_MODULES"/deployments/
	scp "$BUILD_DIR_NG"/public/portal-solicitudes*.war soporte@svr-mj291k6:"$STANDALONE_DIR_PORTAL_MODULES"/deployments/
}

deployAppTAREASPROGRAMADAS() {
	echo $'\nSubiendo TAREAS PROGRAMADAS WAR al JBOSS de PRE (svr-221052d)\n'
	scp "$BUILD_DIR_TAREAS_PROGRAMADAS"/build/libs/stpTTHH-web-1.10.0-RELEASE.ear soporte@svr-221052d:"$STANDALONE_DIR_TAREAS_PROGRAMADAS_GESTAH"/deployments/
}

# desplegar en pre el sistema de nomna-web
deployAppNominaWebDES() {
	echo $'\nSubiendo NOMINA-WEB.WAR al JBOSS de DES (localhost)\n'
	cp /c/Users/mquintosa/tthh-gestah/nomina-web/build/libs/nomina-web* "$DEPLOY_DIR"
}

# desplegar en pre el sistema de nomna-web
deployAppNominaWebPRE() {
	echo $'\nSubiendo NOMINA-WEB.WAR al JBOSS de PRE (svr-221052d)\n'
	scp /c/Users/mquintosa/tthh-gestah/nomina-web/build/libs/nomina-web* soporte@svr-221052d:/opt/rh/jboss/jboss-eap-6.2.3_gestah_1/standalone/deployments/
}

###############################################################################################
#
#			AUTORIZACIONES DE PAGO
#
###############################################################################################

	generateAutorizacionPagoWeb_DES() {
		echo $'\n\nGenerando autorizacion-pago-ng para DES\n'
		cd "$HOME/autorizacion-pago-prestamo-ng"
		#gulp deploy --env dev
		gulp deploy:dist --env dev
	}

	deployAutorizacionPagoWEB_DES() {
		echo $'\nSubiendo WAR autorizacion-pago WEB al JBOSS DESARROLLO\n'
		cp "$HOME/tthh-gestah-autorizacion-pago-ng/dist/gestahAutorizacionPago*" "$DEPLOY_DIR"
	}

	generateAutorizacionPagoWeb_PRE() {
		echo $'\n\nGenerando autorizacion-pago-ng para PRE\n'
		cd "$HOME/tthh-gestah-autorizacion-pago-ng"
		#gulp deploy --env test
		gulp deploy:dist --env test
	}

	deployAutorizacionPagoWEB_PRE() {
		echo $'\nSubiendo WAR autorizacion-pago WEB al JBOSS PRE\n'
		scp "$HOME"/tthh-gestah-autorizacion-pago-ng/dist/gestahAutorizacionPago* soporte@svr-mj0359rv:"$STANDALONE_DIR_GESTAH_PORTAL"/deployments/
	}

	generateAutorizacionPagoWS() {
		echo $'\n'
		DIR="$HOME"/autorizacion-pago-prestamo-root-ws
		if [ -d $DIR ]; then
			cd "$DIR"
			"$GRADLE_DIR31"/gradle clean build -x test
		else
			echo $'No existe el directorio $HOME, por favor revise'
		fi
	}

	deployAutorizacionPagoWS_DES() {
		echo $'\nCopiando WAR en el JBOSS\n'
		cp "$BUILD_DIR_AUTO_PAGO"/autorizacion-pago-prestamo-ws.war "$DEPLOY_DIR"
	}

	deployAutorizacionPagoWS_PRE() {
		echo $'\nSubiendo WAR autorizacion-pago-prestamo-ws al JBOSS PRE\n'
		scp "$BUILD_DIR_AUTO_PAGO"/autorizacion-pago-prestamo-ws.war soporte@svr-mj0359rv:"$STANDALONE_DIR_GESTAH_PORTAL"/deployments/
	}

###############################################################################################

showMainMenu

# ahora buscamos la opcion que se ingreso por los 2 caracteres permitidos teclear