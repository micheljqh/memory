#!/bin/bash

# mostrar menu
mostrarMenu() {
	clear
	read -t120 -p $"
	1) opcion 1
	2) opcion 2
	3) opcion 3
	4) opcion 4
	5) opcion 5
	q) salir
	
	Escriba la opción a ejecutar (separe con espacios para ejecutar varias)? " key

	# inicializa $array dividiendo $key por espacios
	IFS=' ' read -r -a array <<< "$key"

	for element in "${array[@]}"
	do
		ejecutarOpcion $element
	done
	
	# espera por un tecla 3 min
	read -t180 -n1 -p "
	===================================
	Presione una tecla para continuar "
	mostrarMenu
}

# menu para opciones del portal
ejecutarOpcion() {
	case $1 in
		"1")
			echo $'\n\tselecciono la opcion 1'
			;;
		"2")
			echo $'\n\tselecciono la opcion 2'
			;;
		"3")
			echo $'\n\tselecciono la opcion 3'
			;;
		"4")
			echo $'\n\tselecciono la opcion 4'
			;;
		"5")
			echo $'\n\tselecciono la opcion 5'
			;;
		"q")
			echo $'\n\tsalir'
			exit
			;;
		*)
			echo $'\n\tDisculpe, no existe la opcion: "'$1'"'
	esac
}

mostrarMenu
