/**
JSF
<p:watermark for="validationInputText01" value="Number Letter"/>
<s:validationInputText
			id="validationInputText01" label="#{msg_admGas['etiqueta.codigo']}"
			value="#{adminParametrosDataManager.gastoPersonalSelDTO.id.codigoGasto}"
			sizeLabel="120" size="30" required="true"
			onkeypress="return mascaraWebInputNumericLetters(event);"
			requiredMessage="#{msg_admGas['etiqueta.campo.requerido.codigoParametro']}"/>


Si se modifican las dependencias (caso particular) dependencies.ear
 - Compilar y modificar el EAR (actualizar dependencies.ear\lib\fwk-utilitario-aplicacion-1.0.8-RELEASE.jar con el correcto)

*/
 
/**
 * Esta funcion ajusta el texto a 'maxCharacters' por línea,
 * lo mismo que un wrapping
 */
public String ajustarTexto(String texto, int maxCharacters, String changeLine){
	changeLine = "<br/>";
	int newLineEndPos = maxCharacters;
	int newLineStartPos = 0;
	String textoAjustado = "";
	int length = texto.length();

	if (length > maxCharacters) { 
		do {
			if (texto.charAt(newLineEndPos-1) != ' ' && newLineEndPos != length)
				newLineEndPos--;
			else {
				textoAjustado += texto.substring(newLineStartPos, newLineEndPos) + changeLine;
				newLineStartPos = newLineEndPos;
				if (newLineEndPos + maxCharacters > length) newLineEndPos = length;
				else newLineEndPos += maxCharacters;
			}
		}
		while (newLineEndPos != newLineStartPos);
	} else return texto;

	return textoAjustado;
}

/**
ESTAS LINEAS DETERMINA LA MEMORIA USADA
*/

Runtime runtime = Runtime.getRuntime();
System.gc();
long usedMemoryBefore = runtime.totalMemory() - runtime.freeMemory();

/**
AQUI USAR LA MEMORIA
*/

long usedMemoryAfter = runtime.totalMemory() - runtime.freeMemory();
System.out.println("Memory increased:" + (usedMemoryAfter - usedMemoryBefore));
System.out.println(String.format("usedMemoryBefore -> %d %nusedMemoryAfter -> %d", usedMemoryBefore, usedMemoryAfter));

/**
Para que una actionLitener en un portlet funcione debe agregarse esta línea el liferay-portlet.xml
        <requires-namespaced-parameters>false</requires-namespaced-parameters>
*/

/**

FacesContext facesContext = FacesContext.getCurrentInstance();
ExternalContext externalContext = facesContext.getExternalContext();
ActionRequest actionRequest = (ActionRequest) externalContext.getRequest();


Of if using liferay-faces-portal, you can do this:

LiferayFacesContext liferayFacesContext = LiferayFacesContext.getInstance();
ActionRequest actionRequest = (ActionRequest) liferayFacesContext.getPortletRequest();
*/

/*

NoSuchMethodError 
Lanzado si una aplicación intenta llamar a un método específico de una clase (ya sea estática o instancia), y que la clase ya no tiene una definición de ese método.
Normalmente, este error es capturado por el compilador; este error sólo puede ocurrir en tiempo de ejecución si la definición de una clase ha cambiado de manera incompatible.

*/

/**
SPRING DETAILS OF PROJECT RRHH
EN:
prjAdministradorRRHH {
	MADSessionFactoryBeans.xml {
		SE CARGAN TODOS LOS ORM, CON LA DEFINICION DE LAS ENTIDADES DE LOS DISTINTOS MODULOS{
			<value>META-INF/ormServicioPortalRRHH.xml</value>
			<value>META-INF/ormDatoPersonalRRHH.xml</value>
			<value>META-INF/ormPlanAccionista.xml</value>				
			<value>META-INF/ormFramework.xml</value>
			<value>META-INF/ormCorporativo.xml</value>
			<value>META-INF/ormJarimba.xml</value>
		}
	}
}

prjModeloDatoPersonal {
	ormDatoPersonalRRHH.xml {
		SE DEFINEN LAS ENTIDADES QUE SE VAN A MAPEAR PARA ESTE MODELO
	}
}

prjPlanFacilAccionista {
	ormPlanAccionista.xml {
		SE DEFINEN LAS ENTIDADES QUE SE VAN A MAPEAR PARA ESTE MODELO
	}
}

prjModeloServicioPortal {
	ormServicioPortalRRHH.xml {
		SE DEFINEN LAS ENTIDADES QUE SE VAN A MAPEAR PARA ESTE MODELO
	}
}

CUANDO EXISTEN PROBLEMAS DE JAR, LO RECOMENDABLE ES BORRAR EL REPOSITORIO PARA QUE DESCARGUEN OTRA VEZ

*/

/*
Maven error dependencies
- tratar hacerle clean and install al root, si no se puede, quitar los módulos para que actualice el .m2
*/


/* maven utility, avoid test */
mvn clean install -DskipTests

/*

About GC G1

https://www.youtube.com/watch?v=LJC_2agoLuE
https://www.youtube.com/watch?v=UnaNQgzw4zY
https://youtu.be/bhVzCIk3-Q4
http://www.infoq.com/articles/tuning-tips-G1-GC
http://www.infoq.com/articles/G1-One-Garbage-Collector-To-Rule-Them-All
http://www.oracle.com/technetwork/articles/java/g1gc-1984535.html
http://www.mastertheboss.com/jboss-server/jboss-performance/jboss-as-7-performance-tuning

ext
http://liferay1.googlecode.com/svn/trunk/snippet/portal-devt/15-extending-liferay-functionality.txt
https://docs.liferay.com/portal/5.1/javadocs/portal-impl/com/liferay/portal/service/base/UserLocalServiceBaseImpl.java.html
https://docs.liferay.com/portal/5.1/javadocs/portal-impl/com/liferay/portal/service/impl/UserLocalServiceImpl.java.html
https://docs.liferay.com/portal/6.2/propertiesdoc/portal.properties.html

*/

// con spring framework se mejora la injeccion objectos y el mapeo de las tablas, entidades, cuando se ingresa un nuevo ORM que necesitamos
// utilizar debemos incluirlo en el ORM de nuestro proyecto principal

// Definir una propiedad como la que se define en el JBOSS en JAVA
// 	<system-properties>
//    	<property name="org.apache.catalina.connector.URI_ENCODING" value="UTF-8"/>
//      <property name="org.apache.catalina.connector.USE_BODY_ENCODING_FOR_QUERY_STRING" value="true"/>
//      <property name="org.apache.tomcat.util.http.Parameters.MAX_COUNT" value="50000"/>
//      <property name="com.arjuna.ats.arjuna.allowMultipleLastResources" value="true"/>
//      <property name="env" value="DESARROLLO"/>
//      <property name="domain" value="http://localhost:8080"/>
//      <property name="portal.webservices.domain" value="http://localhost:8580"/>
//  </system-properties>

// Eclipse:
// in the tab "Arguments"
// -Dkey=value
// -Ddomain=https://aplpre.favorita.ec
// or
System.getProperties().setProperty("domain", "https://aplpre.favorita.ec");

// manera de obtener el JAR de donde se esta cargando la clase MyClass
MyClass.class.getProtectionDomain().getCodeSource().getLocation().getPath()

// EJ: si quiero saber de que dependencia o JAR esta cargando la clase IOUtils, imprimo la siguiente linea
IOUtils.class.getProtectionDomain().getCodeSource().getLocation().getPath()