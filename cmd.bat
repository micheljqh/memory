﻿REM view path
path

REM temporal change path
setnet us path=%path%;C:\users\mquintosa\appData\Roaming\npm

REM change path in local machine
setx path "%path%;C:\users\mquintosa\appData\Roaming\npm" /m

REM mostrar los servicios en windows
sc queryex type= service state= all | find /i "apache"

REM detener los servicios de windows
net stop apache24
net start apache24