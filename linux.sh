﻿# tanuki construir servicios

# hacer una conexión de red en linux
sudo mkdir /media/liferay

# poner permisos de lectura, escritura y modificación para todos (usuario, grupos, otros)
sudo chmod 777
sudo mount -t cifs //10.200.101.103/liferay /media/liferay -o username=mquintosa,password=Password01,domain=KRUGER

# cambiar a root
sudo -s

# abrir como root
sudo [opcion]

# ejecutar en la consola zsh para mejor ayuda con los comandos
zsh

# folder mysql
/var/lib/mysql

# To list database type the following command
mysql> show databases;
mysql> DROP DATABASE tutorial_database;
mysql> DROP DATABASE IF EXISTS tutorial_database;

# from the prompt command shell, not in mysql command.
# If you want to back up a single database, you merely create the dump and send the output into a file, like so:
mysqldump database_name > database_name.sql

# example
mysqldump -u root -p lportaleeend > lportaleeend.sql

# Multiple databases can be backed up at the same time:
mysqldump --databases database_one database_two > two_databases.sql

# In the code above, database_one is the name of the first database to be backed up, and database_two is the name of the second.
# It is also simple to back up all of the databases on a server:
mysqldump --all-databases > all_databases.sql

# Restoring a Backup
# Since the dump files are just SQL commands, you can restore the database backup by telling mysql to run the commands in it and put the data into the proper database.
mysql -u root -p
mysql> CREATE DATABASE database_name;
mysql> quit
mysql -r root -p database_name < database_name.sql

# In the code above, database_name is the name of the database you want to restore, and database_name.sql is the name of the backup file to be restored..
# If you are trying to restore a single database from dump of all the databases, you have to let mysql know like this:
mysql --one-database database_name < all_databases.sql

# ip de la maquina
ip addr list

#start service jboss
sudo systemctl start liferay_2.service
sudo systemctl stop liferay_2.service

tail -f <file_name>

# CTRL + R Buscar rápido

ip addr show

cambiar de usuario:
su nombre_usuario

#crear usario
useradd <nombre_usuario>

#cambiar password
passwd <nombre_usuario>

Arquiteture of SO
arch

# características de la máquina
free -hd

printenv

# comprimir archivos en linuz c - comprimir v - visualizar z - gz
# Comprimir:
tar -czvf empaquetado.tar.gz /carpeta/a/empaquetar/
# Descomprimir:
tar -xzvf archivo.tar.gz
# Comprimir, ignorar:
tar --exclude='./folder' --exclude='./upload/folder2' -zcvf /backup/filename.tgz /carpeta/a/comprimir

ls -hal -- muestra lista de ficheros y carpetas con sus tamaños

-- ver unidades o discos de linux
sudo lsblk -fm
sudo lsscsi (necesitan instalar este paquete para que el comando esté disponible)
SUDO FDISK -L
df -h
du -sh

# permisos del selinux
getenforce
sestatus

# ver estado, iniciar, detener de un servicio
sudo systemctl status service_name
sudo systemctl start service_name
sudo systemctl stop service_name

# buscar archivos que contienen un texto
# l muestra todos los que cumplen con el match
# i ignora mayusculas y minusculas
# r busca recursivo, hacia adentro del fichero
grep -lir "texto" /path/

# and also, can search with (buscar archivo con texto)
# -r or -R is recursive,
# -n is line number, and
# -w stands for match the whole word.
# -l (lower-case L) can be added to just give the file name of matching files.
grep -rnw '/path' -e 'menu-responsive.css'


# borrar contenido fichero
sudo truncate -s 0 file_name
sudo cp /dev/null file_name

# Rename command syntax
# It is a faster way to group rename files in Linux or Unix-like system. The syntax is:
rename oldname newname *.files

# On most modern Linux distros rename command is known as rename.ul:
rename.ul oldname newname *.files

# For example rename all *.bak file as *.txt, enter:
rename .bak .txt *.bak
rename .undeployed .dodeploy *.undeployed

# Please note that the rename command is part of the util-linux package and can be installed on a Debian or Ubuntu Linux, using the following syntax:
sudo apt-get install renameutils

# renombrar varios aarchivos
rename .undeployed .dodeploy *.undeployed

# cmd 
project to clean\> cls && mvn clean test

hs3js5ly

# CENTOS OPEN FIREWALL PORT 3306
# Use this command to find your active zone(s):
firewall-cmd --get-active-zones
# It will say either public, dmz, or something else. You should only apply to the zones required.
# In the case of dmz try:
firewall-cmd --zone=dmz --add-port=2888/tcp --permanent
# Otherwise, substitute dmz for your zone, for example, if your zone is public:
firewall-cmd --zone=public --add-port=2888/tcp --permanent
# Then remember to reload the firewall for changes to take effect.
firewall-cmd --reload

# conectar al FTP
ftp www.url.com
ftp server-name

# deshabilitar confirmacion
ftp> prompt no

# copiar todos los archivos a la carpeta local, en la que estabas anteriormente
ftp> mget *.*

# contar cantidad de archivos de un directorio
find DIR_NAME -type f | wc -l
find | grep 'regular expresion' # muestra del contenido de la carpeta actual los que coinciden con la expresion regular

# contar la cantidad de lineas de una archivo
wc -l <file_path>

# ver detalles del HDD
df -h

# extrae en la carpeta actual el contenido de la actualizacion o libreria
## For Reference: the cpio arguments are
## 
## -i = extract
## -d = make directories
## -m = preserve modification time
## -v = verbose
rpm2cpio foo.rpm | cpio -idmv

# de la libreria indicada muestra las dependencias
# y si estan instalada en el servidor
ldd libwkhtmltox.so

# view node process
ps aux | grep nodejs
kill -15 25239

# kill nodejs process
pkill -f nodejs

########################################################################################################################

# help: man split
# split [-a suffix_length] [-b byte_count[k|m]] [-l line_count] [-p pattern] [file [name]]

# split file on unix, examples
split -b 1000m image.iso

# split in n=10 files
split -n10 image.iso

# split file with suffix: image...
split -b 1000m image.iso image

# join files after split into filename
cat file1 file2... > image.iso

# see path of process pid
ps auxf | grep ######