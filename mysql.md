```bash
REM install mysql with console
REM INITIALIZE mysql
REM This create [data] folder and content [tables]

mysqld --initialize

REM Install as service in windows
mysqld --install

REM Remove service in windows
mysqld --remove

REM Start service in windows
net start mysql
net stop mysql

REM importar toda la base de datos exportada
REM pararse en la carpeta bin del mysql
REM ejecutar:

mysql -u root -p

REM luego ya dentro del servidor

mysql> create database <nombre_base_dato>;

mysql> quit

REM luego de creada la base de datos, ejecutar para importar en la carpeta bin del mysql:

mysql -u root -p nombre_base_datos < path_script_backup_base_datos
mysqldump -u root -p nombre_base_datos > path_script_backup_base_datos.sql
```